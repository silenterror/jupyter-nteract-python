# Jupyter-Nteract-Python

This repository contains all Python notes in Jupyter format.

## Prerequisites
- pip3 21.3.1
- python 3.9.5
- snapd
    
### Prerequisites Install Resources
- pip    https://pip.pypa.io/en/stable/installation/
- snapd  https://snapcraft.io/docs/installing-snapd
- python https://www.python.org/downloads/

### Jupyter-Nteract Installation
#### Offical Documentation: https://docs.nteract.io/getting-started-with-nteract/

Install nteract via snap

```sh
sudo snap install nteract --edge
```

Install nteract on jupyter

```sh
pip3 install nteract_on_jupyter
```

Clone the repository

```sh
git clone git@gitlab.com:silenterror/jupyter-nteract-python.git
```

Start nteract

```sh
jupyter nteract
```

This will open your browser with the UI.  Browse to the directory you cloned the repo in the UI.
